﻿using System;

namespace RestApi.Models
{
    public class Product
    {
        public Product() { }
        public Product(Product toClone) { Id = toClone.Id; Name = toClone.Name; }
        public Guid Id { get; set; }
        public string Name { get; set; }

        
    }
}