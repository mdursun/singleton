﻿using RestApi.Models;
using RestApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class ProductsController : ApiController
    {
      
        private ProductService service = ProductService.GetSingletonInstance();

        public IHttpActionResult Get()
        {
            var list = service.GetAllProduct();
            //list.Add(new Product { Id = Guid.NewGuid(), Name = "New Prod" }); //Injection Check
            return Ok(list.Count);
        }

        public IHttpActionResult Get(Guid id)
        {
            var product = service.GetAllProduct().Where(p => p.Id == id).SingleOrDefault();
           // product.Name = "Test"; //Illegal Update check
            return Ok(product);
        }

        public IHttpActionResult Post(Product product)
        {
            if (service.GetAllProduct().Where(p => p.Id == product.Id).Any())
                return BadRequest(product.Id.ToString()+" already exists");
            service.AddProduct(product);
            return Ok(product.Id);
        }
        public IHttpActionResult Delete()
        {
            service.DeleteAll();
            return Ok();
        }
    }
}
