﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;
using RestApi.Models;

namespace RestApi.Services
{
    public class ProductService
    {
        private static ProductService service;
        private static readonly object padLock = new object();
        private ConcurrentBag<Product> products = null;

        private ProductService()
        {
            products = new ConcurrentBag<Product>();
        }

        public static ProductService GetSingletonInstance()
        {
            if(service == null)
            {
                lock (padLock)
                {
                    if (service == null)
                        service = new ProductService();
                }
            }
            return service;
        }
        public void AddProduct(Product product)
        {
           
                products.Add(product);
        
            
        }
        public Product GetProduct(Guid id)
        {
            var product = products.SingleOrDefault(p => p.Id == id);
            var clone = new Product(product);


           return clone;
           
        }
        public ConcurrentBag<Product> GetAllProduct()
        {

            var list = new ConcurrentBag<Product>(products.Select(p=> new Product(p)));
           
            return list;
        }
        public void DeleteAll()
        {
          
                products = new ConcurrentBag<Product>();
           
          
        }
    }
}